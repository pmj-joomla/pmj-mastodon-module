<?php
/**
 * @package Module PMJ Mastodon Module for Joomla! 3.8
 * @author PMJ
 * @copyright (C) 2020- PMJ
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/
defined('_JEXEC') or die;

class ModPmjMastodonHelper
{
  public static function getProfile($instance,$accId)
  {
    // start connection
    $ch_mastodon_api_get = curl_init();
    // set options
    curl_setopt($ch_mastodon_api_get, CURLOPT_URL, $instance.'/api/v1/accounts/'.$accId);
    curl_setopt($ch_mastodon_api_get,CURLOPT_RETURNTRANSFER,1);
    // get result
    $profile  = json_decode(curl_exec($ch_mastodon_api_get));
    // close connection
    curl_close ($ch_mastodon_api_get);
    return $profile;
  }
  
  public static function getStatuses($instance,$accId,$params)
  {
    // get limit
    $limit  = $params->get('fetchLimit','20');
    $limit  = !empty($limit) || $limit !== '0' || (int)$limit <= 50 ? '?limit='.$limit : '' ;
    // start connection
    $ch_mastodon_api_get = curl_init();
    // set options
    curl_setopt($ch_mastodon_api_get, CURLOPT_URL, $instance.'/api/v1/accounts/'.$accId.'/statuses'.$limit);
    curl_setopt($ch_mastodon_api_get,CURLOPT_RETURNTRANSFER,1);
    // get result
    $statuses  = json_decode(curl_exec($ch_mastodon_api_get));
    // close connection
    curl_close ($ch_mastodon_api_get);
    
    // compile statuses
    $compiled = array();
    // visibility
    $visibility = array();
    switch ($params->get('visibilityLevel','public'))
    {
      case 'unlisted':
        $visibility[] = 'unlisted';
      case 'public':
        $visibility[] = 'public';
    }
    foreach ($statuses as $status)
    {
      $isReply = $status->in_reply_to_id ? true : false;
      $isReblog = $status->reblog ? true : false;
      
      if ( ( in_array($status->visibility, $visibility) || in_array($status->reblog->visibility,$visibility) ) && ( ($isReply && $params->get('showReply',0)) || !$isReply ) && ( ($isReblog && $params->get('showReblog',0)) || !$isReblog ) )
      {
        $compiled[] = $status;
      }
    }
    
    return array_slice($compiled, 0, $params->get('tootLimit','5'));
  }
  
  public static function getUserID($instance,$account,$params)
  {
    // get token
    $token  = $params->get('token');
    // set headers
    $headers = array('Authorization: Bearer '. $token);
    // start connection
    $ch_mastodon_api_get = curl_init();
    // set options
    curl_setopt($ch_mastodon_api_get, CURLOPT_URL, $instance.'/api/v2/search?q='.$account.'&resolve=false&type=accounts');
    curl_setopt($ch_mastodon_api_get, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch_mastodon_api_get,CURLOPT_RETURNTRANSFER,1);
    // get result
    $accounts  = json_decode(curl_exec($ch_mastodon_api_get));
    // close connection
    curl_close ($ch_mastodon_api_get);
    // parse result and get id
    foreach ($accounts->accounts as $acc)
    {
      if ($acc->url == $instance.'/@'.$acc->username)
      {
        return $acc->id;
      }
    }
  }
  
  public static function diffTime($time)
  {
    $time = time() - strtotime($time); // to get the time since that moment
    $time = ($time<1)? 1 : $time;
    $tokens = array (
      31536000 => 'y',
      2592000 => 'm',
      604800 => 'w',
      86400 => 'd',
      3600 => 'h',
      60 => 'm',
      1 => 's'
    );

    foreach ($tokens as $unit => $text)
    {
      if ($time < $unit) continue;
      $numberOfUnits = floor($time / $unit);
      return $numberOfUnits.$text/*.(($numberOfUnits>1)?'s':'')*/;
    }
  }
}
