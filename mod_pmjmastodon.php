<?php
/**
 * @package Module PMJ Mastodon Module for Joomla! 3.8
 * @author PMJ
 * @copyright (C) 2020- PMJ
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/
defined('_JEXEC') or die;

// include helper
JLoader::register('ModPmjMastodonHelper', __DIR__ . '/helper.php');

// add a small css declaration for screen readers
$doc	= JFactory::getDocument();
if ($params->get('enableFontawesome',0))
{
  $doc->addStyleSheet('https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array('version' => 'auto', 'relative' => true));
}
if ($params->get('enableBootstrap',0))
{
  $doc->addStyleSheet('https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css', array('version' => 'auto', 'relative' => true));
}
if ($params->get('enableScroll',0))
{
  $doc->addStyleDeclaration('#pmj-mastodon-module{overflow-y:scroll;max-height:'.$params->get('scrollHeight','50em').';}');
}
$doc->addStyleDeclaration('.invisible{position:absolute;width:1px;height:1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);white-space:nowrap;border:0}');
$doc->addStyleDeclaration('#pmj-mastodon-module{font-size:'.$params->get('fontSize','0.8em').';}');

// get account
$account  = $params->get('account','pmj@social.pmj.rocks');
// get instance
$instance  = explode('@',$account);
$instance = 'https://'.$instance[1];

$accId  = ModPmjMastodonHelper::getUserID($instance,$account,$params);
$profile  = ModPmjMastodonHelper::getProfile($instance,$accId);
$statuses = ModPmjMastodonHelper::getStatuses($instance,$accId,$params);

$layout	= $params->get('layout', 'default');

require JModuleHelper::getLayoutPath('mod_pmjmastodon', $layout);
