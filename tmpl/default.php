<?php
/**
 * @package Module PMJ Mastodon Module for Joomla! 3.8
 * @author PMJ
 * @copyright (C) 2020- PMJ
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/
defined('_JEXEC') or die;
?>
<div id="pmj-mastodon-module">
<?php if ($profile && $params->get('showProfile',1)) : ?>
<div class="card my-1 bg-secondary text-dark border-primary">
  <div class="card-body">
    <div class="row no-gutters">
      <div class="col-md-4">
        <a href="<?php echo $profile->url; ?>" target="_blank" rel="me noopener noreferrer external"><img src="<?php echo $profile->avatar_static; ?>" class="card-img" alt="<?php echo '@'.$profile->acct . '@'.$instance; ?>"></a>
      </div>
      <div class="col-md-8 pl-1">
        <h5 class="card-title"><?php echo $profile->display_name; ?></h5>
      </div>
    </div>
    <div class="row">
      <div class="col">
      <p class="text-muted"><a href="<?php echo $profile->url; ?>" target="_blank" rel="me noopener noreferrer external"><?php echo '@'.$account; ?></a></p>
      </div>
    </div>
    <?php if ($params->get('showProfileDescription',1) || $params->get('showProfileStats',1)) : ?>
    <div class="row">
      <div class="col">
        <?php if ($params->get('showProfileDescription',1)) : ?>
        <?php echo $profile->note; ?>
        <?php endif; ?>
        <?php if ($params->get('showProfileStats',1)) : ?>
        <p><?php echo $profile->statuses_count; ?> Toots | <?php echo $profile->followers_count; ?> Followers | <?php echo $profile->following_count; ?> Following</p>
        <?php endif; ?>
      </div>
    </div>
    <?php endif; ?>
  </div>
</div>
<?php endif; ?>
<?php if ($statuses) : ?>
<?php foreach ($statuses as $status) : ?>
<div class="card border-primary my-1">
  <div class="card-header border-primary">
    <div class="d-flex row justify-content-between">
      <?php if ($status->reblog) : ?>
      <div class="col-9"><a href="<?php echo $status->reblog->account->url; ?>" target="_blank" rel="noopener noreferrer external"><?php echo $status->reblog->account->display_name; ?></a></div>
      <?php else : ?>
      <div><?php echo $status->account->display_name; ?></div>
      <?php endif; ?>
      <div>
        <?php if ($status->in_reply_to_id) : ?>
        <span class="badge"><i class="fa fa-reply"></i></span> 
        <?php endif; ?>
        <?php if ($status->reblog) : ?>
        <span class="badge"><i class="fa fa-retweet fa-lg"></i></span> 
        <?php endif; ?>
        <a href="<?php echo $instance.'/web/statuses/'.$status->id; ?>" target="_blank" rel="noopener noreferrer external">
          <time datetime="<?php echo $status->created_at; ?>" title="<?php echo date('M j, Y, H:i',strtotime($status->created_at)); ?>">
            <?php echo ModPmjMastodonHelper::diffTime($status->created_at); ?>
          </time>
        </a>
      </div>
    </div>
  </div>
  <div class="card-body">
    <div><?php echo $status->reblog ? $status->reblog->content :  $status->content; ?></div>
    <div>
    <?php if (!empty($status->media_attachments || $status->reblog->media_attachments)) : ?>
      <?php $statusMedia = $status->media_attachments ? $status->media_attachments : array(); ?>
      <?php $statusReblog = $status->reblog->media_attachments ? $status->reblog->media_attachments : array(); ?>
      <?php $media = $statusMedia  + $statusReblog; ?>
      <?php foreach ($media as $media_attachment) : ?>
        <?php if ($media_attachment->type == 'image') : ?>
        <img class="img img-fluid" src="<?php echo $media_attachment->preview_url; ?>" />
        <?php endif; ?>
      <?php endforeach; ?>
    <?php endif; ?>
    </div>
    <hr class="border-secondary" />
    <small class="text-muted">
      Link: <a href="<?php echo $instance.'/web/statuses/'.$status->id; ?>" target="_blank" rel="noopener noreferrer external"><?php echo $instance.'/web/statuses/'.$status->id; ?></a>
    </small>
  </div>
  <div class="card-footer border-primary">
    <i class="fa fa-reply fa-lg"></i> <span class="badge"><?php echo $status->reblog ? $status->reblog->replies_count : $status->replies_count; ?></span>
    <i class="fa fa-retweet fa-lg"></i> <span class="badge"><?php echo $status->reblog ? $status->reblog->reblogs_count : $status->reblogs_count; ?></span>
    <i class="fa fa-<?php echo $status->favourited || $status->reblog->favourited ? 'star' : 'star-o'; ?> fa-lg"></i> <span class="badge"><?php echo $status->reblog ? $status->reblog->favourites_count : $status->favourites_count; ?></span>
  </div>
</div>
<?php endforeach; ?>
<?php else : ?>
<div class="card border-primary my-1">
  <div class="card-body">
  <?php echo JText::_('MOD_PMJMASTODON_NO_TOOTS'); ?>
  </div>
</div>
<?php endif; ?>
</div>
