# PMJ Mastodon

PMJ Mastodon is a very simple Joomla module to display the latest posts from a [Mastodon](https://joinmastodon.org/) account.  
You need to create an app for your account on your instance!

Works with all Joomla Versions 3.8+

## Table of contents

* [Downloads](#downloads)
* [Demo](#demo)
* [Override](#override)
* [Paramters](#paramters)

## Downloads

**[Current Stable Version (1.1.3)](https://gitlab.com/pmj-joomla/pmj-mastodon-module/-/archive/1.1.3/pmj-mastodon-module-1.1.3.zip)**

**[Developement Version](https://gitlab.com/pmj-joomla/pmj-mastodon-module/-/archive/dev/pmj-mastodon-module-dev.zip)**

## Demo

You can see a working example in a production environment on the [PMJ Rocks Website](https://pmj.rocks)

**Note**: The layout has been built with Bootstrap 4 and FontAwesome 4.7 because I use the [PMJ Bootstrap Template](https://gitlab.com/pmj-joomla/pmj-bootstrap-template) but you can always create an [Override](#override) for your template

## How to use it

First, you need to **create an app** in the developement section of **your instance**.  
![Create new application](readme/NewApplication.png "Create new application")  
![Application settings](readme/NewApplicationSettings.png "Application settings")  
The **scope** has to include at least **read:accounts**, **read:search** and **read:statuses**!!

Then you need to copy the **access token** and insert it into the token field in the module's backend and insert your full Mastodon username (user@instance.domain)  
![Get access token](readme/AccessToken.png "Get access token")  
![Set access token](readme/AccessTokenModule.png "Set access token")

Yay, your latest toots are now displayed in Joomla!

## Override

### Variables

There are two variables which can be used for overrides in the template (default.php)

* `(object) $profile`  
this is the account object from the Mastodon API  
check the [API](https://docs.joinmastodon.org/entities/account/) for further reference
* `(object) $statuses`  
this holds all the latest status updates from the Mastodon API  
check the [API](https://docs.joinmastodon.org/entities/status/) for further reference

There are also some paramters transfered into variables which are available to the view

* `(string) $account`  
this is the account you'll define in the module backend, prepended with @
* `(string) $instance`  
this is the domain part from $account, prepended with https://

## Parameters

A list of all paramters  and their return values which can be set in the backend and how they are used by default.  
Values in bold are default settings.  

### Basic

* `token` (text)  
the access token from your Mastodon App
* `account` (text)  
your username user@instance.domain
* `tootLimit` (text - **5**)  
how many toots to list
* `showProfile` (**Yes**/No)  
show the header with profile information (avatar, display name and username)
* `showProfileDescription` (**Yes**/No)  
show your profile description
* `showProfileStats` (**Yes**/No)  
show stats (toots, follower and following count)

### Design

* `enableFontawesome` (**Yes**/No)  
load the FontAwesome 4.7 framework  
**Note** this is loaded from an external CDN and only needed if you don't use a template with this framework
* `enableBootstrap` (**Yes**/No)  
load the Bootstrap 4.5 framework (only css)  
**Note** this is loaded from an external CDN and only needed if you don't use a template with this framework
* `enableScroll` (**Yes**/No)  
enable scrolling of the container if you set a height which is smaller than the list of toots display
* `scrollHeight` (text - **50em**)  
the height of the container
* `fontSize` (text - **0.8em**)  
set the fontsize for the container
