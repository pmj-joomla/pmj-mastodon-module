## Changelog

### 1.1

#### 1.1.3 [2024-05-05]

* Version 1.1.3
* Try to fix the language file installation

#### 1.1.2 [2024-05-05]

* Version 1.1.2
* Fixed counters for reblog items

#### 1.1.1 [2024-05-04]

* Version 1.1.1
* Fixed content display of reblogs and small design improvement

#### 1.1.0 [2020-10-29]

* Version 1.1.0
* Updated display and added target blank and rel to links
* Added notification if no toots are available
* **[BUG]** Link whole toot didn't work
* **[BUG}** Another fix for media attachements
* Added filters to filter out replies, reblogs, public, unlisted
* **[BUG]** Display images from reblogs

---
### 1.0

#### 1.0.0 [2020-08-04]

* Version 1.0.0
* Updated readme
* Moved $token and $limit out of the view scope
* Added some parameters
* Basic design
* Added time function
* Get profile
* Clean up code
* Changed all files to linux linebreak
* Get Statuses
* Get account ID
* Added basic paramters
* Initial Commit
